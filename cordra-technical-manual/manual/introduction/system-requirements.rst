System Requirements
===================

Cordra software can be deployed in at least two ways: as a single instance or as a
distributed system. The full extent of system requirements depends on the deployment option
selected. Please refer to these two documents for learning about those requirements:
:doc:`../configuration/single-instance-deployment` and :doc:`../configuration/distributed-deployment`.

The only (virtual) machine instance in the case of a single instance deployment or each instance in
the case of a distributed deployment, is expected to meet the following requirements:

======================  =======================================
Minimum                 Recommended
======================  =======================================
JVM: Java version 8     JVM: Java version 8 or later
CPU: Single core 1GHz   CPU: Single core 2GHz
RAM: 500MB              RAM: 2GB
HDD: 200MB              HDD: Storage dependent
======================  =======================================

Storage is used for just logging system access if external backend services are employed.
Otherwise, required storage space is dependent on the number and size of digital objects that are
stored and indexed as well as the number of user requests. In general, and up to a certain point,
more system memory will yield better search performance.
