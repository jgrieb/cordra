APIs
====

.. toctree::
   :maxdepth: 1
   :glob:

   introduction
   rest-api
   doip
   doip-api-for-http-clients
   search
