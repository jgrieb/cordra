Extensions and Applications
===========================

.. toctree::
   :maxdepth: 1
   :glob:

   document-repository
   medical-records-application
   person-registry
   user-registration
   object-linking
   oai-pmh
   recommendations
   sending-emails
