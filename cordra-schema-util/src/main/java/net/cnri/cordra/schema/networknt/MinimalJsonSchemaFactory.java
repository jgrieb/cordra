package net.cnri.cordra.schema.networknt;

import com.networknt.schema.*;

import net.cnri.cordra.schema.SchemaResolver;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MinimalJsonSchemaFactory {

    public static final String URI = "https://cordra.org/schema";

    public static class Builder {
        private final Set<Keyword> additionalKeywords = new HashSet<>();
        private final Set<Format> additionalFormats = new HashSet<>();

        public Builder withAdditionalKeyword(Keyword keyword) {
            this.additionalKeywords.add(keyword);
            return this;
        }

        public Builder withAdditionalKeywords(List<Keyword> keywords) {
            this.additionalKeywords.addAll(keywords);
            return this;
        }

        public Builder withAdditionalFormat(Format format) {
            this.additionalFormats.add(format);
            return this;
        }

        public Builder withAdditionalFormats(List<Format> formats) {
            this.additionalFormats.addAll(formats);
            return this;
        }

        public JsonSchemaFactory build() {
            JsonMetaSchema metaSchema = JsonMetaSchema.builder(URI)
                    .idKeyword("id") // this influences the behavior of $ref
                    .addKeyword(new ParentInfoCachingKeyword(ValidatorTypeCode.ADDITIONAL_PROPERTIES))
                    .addKeyword(new ParentInfoCachingKeyword(ValidatorTypeCode.CONTAINS))
                    .addKeyword(new ParentInfoCachingKeyword(ValidatorTypeCode.ITEMS))
                    .addKeyword(new ParentInfoCachingKeyword(ValidatorTypeCode.PATTERN_PROPERTIES))
                    .addKeyword(new ParentInfoCachingKeyword(ValidatorTypeCode.PROPERTIES))
                    .addKeyword(ValidatorTypeCode.ALL_OF)
                    .addKeyword(ValidatorTypeCode.ANY_OF)
                    .addKeyword(ValidatorTypeCode.DEPENDENCIES)
                    .addKeyword(ValidatorTypeCode.IF_THEN_ELSE)
                    .addKeyword(ValidatorTypeCode.REF)
                    .addKeyword(ValidatorTypeCode.NOT)
                    .addKeyword(ValidatorTypeCode.ONE_OF)
                    .addKeyword(ValidatorTypeCode.TYPE)
                    .addKeyword(ValidatorTypeCode.UNION_TYPE)
                    .addKeywords(additionalKeywords)
                    .addFormats(additionalFormats)
                    .build();

            return new JsonSchemaFactory.Builder()
                    .defaultMetaSchemaURI(metaSchema.getUri())
                    .addMetaSchema(metaSchema)
                    .build();
        }

        public JsonSchemaFactory buildWithSchemaResolver(SchemaResolver resolver) {
            JsonSchemaFactory factory = build();
            NetworkntJsonSchemaUtil.setSchemaResolver(factory, resolver);
            return factory;
        }
    }
}
