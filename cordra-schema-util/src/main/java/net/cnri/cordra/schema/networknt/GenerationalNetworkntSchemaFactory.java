package net.cnri.cordra.schema.networknt;

import java.util.concurrent.atomic.AtomicLong;
import com.fasterxml.jackson.databind.JsonNode;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonSchemaFactory;

import net.cnri.cordra.InvalidException;
import net.cnri.cordra.schema.GenerationalSchemaFactory;

public class GenerationalNetworkntSchemaFactory implements GenerationalSchemaFactory<JsonSchema> {

    private final JsonSchemaFactory factory;
    private final AtomicLong generation = new AtomicLong(0);
    private volatile long factoryGeneration = -1;

    public GenerationalNetworkntSchemaFactory(JsonSchemaFactory factory) {
        this.factory = factory;
    }

    @Override
    public long incrementAndGetGeneration() {
        return generation.incrementAndGet();
    }

    @Override
    public long getGeneration() {
        return generation.get();
    }

    @Override
    public SchemaAndGeneration<JsonSchema> getSchema(JsonNode schemaNode, String baseUri) throws InvalidException {
        if (factoryGeneration < generation.get()) {
            clearCache();
        }
        long currentGen = factoryGeneration;
        JsonSchema schema = NetworkntJsonSchemaUtil.getJsonSchema(factory, schemaNode, baseUri);
        return new SchemaAndGeneration<>(schema, currentGen);
    }

    private synchronized void clearCache() {
        // double-checked locking
        if (factoryGeneration >= generation.get()) return;
        NetworkntJsonSchemaUtil.clearCache(factory);
        factoryGeneration = generation.get();
    }
}
