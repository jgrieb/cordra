package net.cnri.cordra.schema.fge;

import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Supplier;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

import net.cnri.cordra.InvalidException;
import net.cnri.cordra.schema.GenerationalSchemaFactory;

public class GenerationalFgeSchemaFactory implements GenerationalSchemaFactory<JsonSchema> {

    private final Supplier<JsonSchemaFactory> factorySupplier;
    private final AtomicLong generation = new AtomicLong(0);
    private volatile long factoryGeneration = -1;
    private JsonSchemaFactory factory;

    // for fge, clearing the cache really means starting over with a new factory
    public GenerationalFgeSchemaFactory(Supplier<JsonSchemaFactory> factorySupplier) {
        this.factorySupplier = factorySupplier;
    }

    @Override
    public long incrementAndGetGeneration() {
        return generation.incrementAndGet();
    }

    @Override
    public long getGeneration() {
        return generation.get();
    }

    @Override
    public SchemaAndGeneration<JsonSchema> getSchema(JsonNode schemaNode, String baseUri) throws InvalidException {
        if (factoryGeneration < generation.get()) {
            updateCachedFactory();
        }
        long currentGen = factoryGeneration;
        JsonSchema schema = FgeJsonSchemaUtil.getJsonSchema(factory, schemaNode, baseUri);
        return new SchemaAndGeneration<>(schema, currentGen);
    }

    private synchronized void updateCachedFactory() {
        // double-checked locking
        if (factoryGeneration >= generation.get()) return;
        factory = factorySupplier.get();
        factoryGeneration = generation.get();
    }
}
