package net.cnri.cordra.schema;

@FunctionalInterface
public interface SchemaResolver {
    SchemaNodeAndUri resolve(String uri);
}