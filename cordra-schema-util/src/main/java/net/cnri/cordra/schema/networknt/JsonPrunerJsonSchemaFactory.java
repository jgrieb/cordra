package net.cnri.cordra.schema.networknt;

import com.networknt.schema.JsonSchemaFactory;
import com.networknt.schema.ValidatorTypeCode;

import net.cnri.cordra.schema.SchemaResolver;

public class JsonPrunerJsonSchemaFactory {
    private static MinimalJsonSchemaFactory.Builder BUILDER = new MinimalJsonSchemaFactory.Builder()
        .withAdditionalKeyword(new JsonPrunerDelegatingKeyword(new ParentInfoCachingKeyword(ValidatorTypeCode.PROPERTIES)))
        .withAdditionalKeyword(new JsonPrunerDelegatingKeyword(new ParentInfoCachingKeyword(ValidatorTypeCode.PATTERN_PROPERTIES)))
        .withAdditionalKeyword(new JsonPrunerDelegatingKeyword(new ParentInfoCachingKeyword(ValidatorTypeCode.ADDITIONAL_PROPERTIES)));

    public static JsonSchemaFactory withSchemaResolver(SchemaResolver schemaResolver) {
        JsonSchemaFactory res = BUILDER.build();
        NetworkntJsonSchemaUtil.setSchemaResolver(res, schemaResolver);
        return res;
    }
}
