package net.cnri.cordra.schema.networknt;

import com.fasterxml.jackson.databind.JsonNode;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonValidator;
import com.networknt.schema.Keyword;
import com.networknt.schema.ValidationContext;

public class JsonPrunerDelegatingKeyword implements Keyword {

    private final Keyword delegate;

    public JsonPrunerDelegatingKeyword(Keyword delegate) {
        this.delegate = delegate;
    }

    @Override
    public String getValue() {
        return delegate.getValue();
    }

    @Override
    public JsonValidator newValidator(String schemaPath, JsonNode schemaNode, JsonSchema parentSchema, ValidationContext validationContext) throws Exception {
        return new JsonPrunerDelegatingJsonValidator(delegate.newValidator(schemaPath, schemaNode, parentSchema, validationContext), getValue(), parentSchema);
    }

}
