package net.cnri.cordra;

public class CordraHooksSupportProvider {

    private static volatile CordraHooksSupport cordraHooksSupport;

    public static CordraHooksSupport get() {
        return cordraHooksSupport;
    }

    public static void set(CordraHooksSupport chs) {
        cordraHooksSupport = chs;
    }
}
