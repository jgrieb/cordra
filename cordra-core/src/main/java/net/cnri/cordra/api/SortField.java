package net.cnri.cordra.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.gson.reflect.TypeToken;

import net.cnri.cordra.util.GsonUtility;

public class SortField {
    private final String name;
    private final boolean reverse;

    public SortField(String name, boolean reverse) {
        this.name = name;
        this.reverse = reverse;
    }

    public SortField(String name) {
        this(name, false);
    }

    public String getName() {
        return name;
    }

    public boolean isReverse() {
        return reverse;
    }

    @Override
    public String toString() {
        if (reverse) return name + " DESC";
        else return name;
    }

    public static List<SortField> getSortFieldsFromString(String sortFields) {
        if (sortFields == null || "".equals(sortFields)) {
            return null;
        } else if (sortFields.trim().startsWith("[")) {
            return GsonUtility.getGson().fromJson(sortFields, new TypeToken<List<SortField>>() {}.getType());
        } else {
            List<SortField> result = new ArrayList<>();
            List<String> sortFieldStrings = getFieldsFromString(sortFields);
            for (String sortFieldString : sortFieldStrings) {
                result.add(getSortFieldFromString(sortFieldString));
            }
            return result;
        }
    }

    public static SortField getSortFieldFromString(String sortFieldString) {
        String[] terms = sortFieldString.split(" ");
        boolean reverse = false;
        if (terms.length > 1) {
            String direction = terms[1];
            if ("DESC".equalsIgnoreCase(direction)) reverse = true;
        }
        String fieldName = terms[0];
        return new SortField(fieldName, reverse);
    }

    private static List<String> getFieldsFromString(String s) {
        return Arrays.asList(s.split(", *"));
    }
}
