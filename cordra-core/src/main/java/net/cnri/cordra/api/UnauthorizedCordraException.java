package net.cnri.cordra.api;

import com.google.gson.JsonElement;

public class UnauthorizedCordraException extends CordraException {

    public UnauthorizedCordraException(String message, Throwable cause) {
        super(CordraException.responseForMessage(message), 401, cause);
    }

    public UnauthorizedCordraException(String message) {
        super(CordraException.responseForMessage(message), 401);
    }

    public UnauthorizedCordraException(Throwable cause) {
        super(401, cause);
    }

    public UnauthorizedCordraException(JsonElement response, Throwable cause) {
        super(response, 401, cause);
    }

    public UnauthorizedCordraException(JsonElement response) {
        super(response, 401);
    }
}
