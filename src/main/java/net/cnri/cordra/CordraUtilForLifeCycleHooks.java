package net.cnri.cordra;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.CordraObject;
import net.cnri.cordra.api.InternalErrorCordraException;
import net.cnri.cordra.storage.CordraStorage;
import net.cnri.cordra.util.SearchUtil;
import net.handle.hdllib.trust.JsonWebSignature;
import net.handle.hdllib.trust.JsonWebSignatureFactory;
import net.handle.hdllib.trust.TrustException;

import java.security.PrivateKey;
import java.security.PublicKey;

public class CordraUtilForLifeCycleHooks {

    private CordraService cordraService;
    private Gson gson;

    public void init(@SuppressWarnings("hiding") CordraService cordraService) {
        this.cordraService = cordraService;
        this.gson = GsonUtility.getGson();
    }

    public String hashJson(String jsonString, String algorithm) {
        if (jsonString == null || jsonString.isEmpty()) return null;
        JsonElement el = JsonParser.parseString(jsonString);
        return CordraObjectHasher.hashJson(el, algorithm);
    }

    public String escapeForQuery(String s) {
        return SearchUtil.escape(s);
    }

    public String verifySecret(String objectJson, String jsonPointer, String secret) {
        CordraObject cordraObject = gson.fromJson(objectJson, CordraObject.class);
        return String.valueOf(cordraService.verifySecureProperty(cordraObject, jsonPointer, secret));
    }

    public String verifyHashes(String coJson) throws CordraException {
        CordraObject co = gson.fromJson(coJson, CordraObject.class);
        CordraStorage storage = cordraService.storage;
        CordraObjectHasher hasher = new CordraObjectHasher();
        CordraObjectHasher.VerificationReport report = hasher.verify(co, storage);
        String result = gson.toJson(report);
        return result;
    }

    public String signWithKey(String payload, String privateKey, boolean useJsonSerialization) throws TrustException, CordraException {
        if (privateKey == null) {
            throw new InternalErrorCordraException("No key provided for signing");
        }
        if (payload == null) {
            throw new InternalErrorCordraException("No payload provided for signing");
        }
        PrivateKey key = gson.fromJson(privateKey, PrivateKey.class);
        JsonWebSignature jws = JsonWebSignatureFactory.getInstance().create(payload, key);
        if (useJsonSerialization) {
            return jws.serializeToJson();
        } else {
            return jws.serialize();
        }
    }

    public String signWithCordraKey(String payload, boolean useJsonSerialization) throws TrustException, CordraException {
        PrivateKey privateKey = cordraService.privateKey;
        if (privateKey == null) {
            throw new InternalErrorCordraException("No key available for signing");
        }
        if (payload == null) {
            throw new InternalErrorCordraException("No payload provided for signing");
        }
        JsonWebSignature jws = JsonWebSignatureFactory.getInstance().create(payload, privateKey);
        if (useJsonSerialization) {
            return jws.serializeToJson();
        } else {
            return jws.serialize();
        }
    }

    public boolean verifyWithCordraKey(String jwt) throws TrustException {
        PublicKey publicKey = cordraService.publicKey;
        JsonWebSignatureFactory signatureFactory = JsonWebSignatureFactory.getInstance();
        JsonWebSignature jws = signatureFactory.deserialize(jwt);
        String payloadJson = jws.getPayloadAsString();
        JsonObject claims = JsonParser.parseString(payloadJson).getAsJsonObject();
        if (!checkClaims(claims)) {
            return false;
        }
        boolean isValid = jws.validates(publicKey);
        return isValid;
    }

    public String extractJwtPayload(String jwt) throws TrustException {
        JsonWebSignatureFactory signatureFactory = JsonWebSignatureFactory.getInstance();
        JsonWebSignature jws = signatureFactory.deserialize(jwt);
        String payloadJson = jws.getPayloadAsString();
        return payloadJson;
    }


    private boolean checkClaims(JsonObject claims) {
        if (!claims.has("iss") || !claims.has("exp")) {
            // need an issuer
            // need an expiration date (prevent footguns)
            return false;
        }
        String iss = claims.get("iss").getAsString();
        JsonObject doipConfig = cordraService.getDoipProcessorConfig();
        String serviceId = doipConfig.get("serviceId").getAsString();
        if (!iss.equals(serviceId)) {
            return false;
        }
        long exp = claims.get("exp").getAsLong();
        long nowInSeconds = System.currentTimeMillis() / 1000L;
        if (nowInSeconds > exp) {
            return false;
        }
        long nbf = 0;
        if (claims.has("nbf")) {
            nbf = claims.get("nbf").getAsLong();
        }
        if (nowInSeconds + 300 < nbf) {
            // 5 minute clock skew for nbf
            return false;
        }
        return true;
    }


    public String getCordraPublicKey() {
        return gson.toJson(cordraService.publicKey);
    }

    public String getDoipProcessorConfig() {
        JsonObject doipConfig = cordraService.getDoipProcessorConfig();
        String result = gson.toJson(doipConfig);
        return result;
    }

}
