package net.cnri.cordra.auth;

import net.cnri.cordra.CordraService;
import net.cnri.cordra.api.BadRequestCordraException;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.CordraObject;
import net.cnri.cordra.api.NotFoundCordraException;
import net.cnri.cordra.indexer.CordraIndexer;
import net.cnri.cordra.indexer.IndexerException;
import net.cnri.cordra.storage.CordraStorage;
import net.cnri.cordra.util.SearchUtil;

import javax.script.ScriptException;
import java.util.*;
import java.util.stream.Collectors;

public class AclEnforcer {

    private final static List<String> LEGACY_CALL_PERMISSIONS = Collections.singletonList("writers");

    private final CordraService cordra;
    private final CordraStorage storage;
    private final CordraIndexer indexer;
    private final AuthCache authCache;
    private volatile AuthConfig authConfig;

    public enum Permission {
        NONE,
        READ, //cannot read payloads
        READ_INCLUDING_PAYLOADS,
        WRITE
    }

    public AclEnforcer(CordraService cordra, CordraStorage storage, CordraIndexer indexer, AuthCache authCache) {
        this.cordra = cordra;
        this.storage = storage;
        this.indexer = indexer;
        this.authCache = authCache;
    }

    public Permission permittedOperations(String userId, boolean hasUserObject, String objectId) throws CordraException {
        if ("admin".equals(userId)) return Permission.WRITE;
        CordraObject co = storage.get(objectId);
        return permittedOperations(userId, hasUserObject, co);
    }

    private Permission permittedOperationsForUser(String userId, boolean hasUserObject, CordraObject co) {
        boolean canWrite = canWrite(co, userId, hasUserObject);
        if (canWrite) return Permission.WRITE;
        boolean canRead = canReadOnly(co, userId, hasUserObject);
        boolean canReadIncludingPayloads = canRead && canReadIncludingPayloads(co, userId, hasUserObject);
        if (canReadIncludingPayloads) return Permission.READ_INCLUDING_PAYLOADS;
        if (canRead) return Permission.READ;
        return Permission.NONE;
    }

    public Permission permittedOperations(String userId, boolean hasUserObject, CordraObject co) throws CordraException {
        if ("admin".equals(userId)) return Permission.WRITE;
        Permission res = permittedOperationsForUser(userId, hasUserObject, co);
        if (Permission.WRITE == res) return res;
        List<String> groups = getGroupsForUser(userId);
        for (String group : groups) {
            Permission groupPerm = permittedOperationsForUser(group, hasUserObject, co);
            if (doesPermissionAllowOperation(groupPerm, res)) res = groupPerm;
            if (Permission.WRITE == res) return res;
        }
        return res;
    }

    public Permission permittedOperationsForUserAndGroups(String userId, boolean hasUserObject, List<String> groups, CordraObject co) {
        if ("admin".equals(userId)) return Permission.WRITE;
        Permission res = permittedOperationsForUser(userId, hasUserObject, co);
        if (Permission.WRITE == res) return res;
        for (String group : groups) {
            Permission groupPerm = permittedOperationsForUser(group, hasUserObject, co);
            if (doesPermissionAllowOperation(groupPerm, res)) res = groupPerm;
            if (Permission.WRITE == res) return res;
        }
        return res;
    }

    public boolean canRead(String userId, boolean hasUserObject, CordraObject co) throws CordraException {
        Permission perm = permittedOperations(userId, hasUserObject, co);
        return perm != Permission.NONE;
    }

    public static boolean doesPermissionAllowOperation(Permission permission, Permission requiredPermission) {
        if (requiredPermission == Permission.NONE) {
            return true;
        } else if (requiredPermission == Permission.READ) {
            return permission == Permission.READ || permission == Permission.READ_INCLUDING_PAYLOADS || permission == Permission.WRITE;
        } else if (requiredPermission == Permission.READ_INCLUDING_PAYLOADS) {
            return permission == Permission.READ_INCLUDING_PAYLOADS || permission == Permission.WRITE;
        } else if (requiredPermission == Permission.WRITE) {
            return permission == Permission.WRITE;
        } else {
            throw new AssertionError();
        }
    }

    public void setAuthConfig(AuthConfig authConfig) {
        this.authConfig = authConfig;
    }

    private boolean isPermittedToCreateForUser(String userId, boolean hasUserObject, String objectType) {
        if ("admin".equals(userId)) return true;
        DefaultAcls acls = authConfig.getAclForObjectType(objectType);
        if (acls == null || acls.aclCreate == null) return false;
        for (String permittedId : acls.aclCreate) {
            if ("public".equals(permittedId)) return true;
            if (userId != null && hasUserObject && "authenticated".equals(permittedId)) return true;
            if (permittedId != null && permittedId.equalsIgnoreCase(userId)) return true;
        }
        return false;
    }

    private List<String> combineOnlyUniqueItems(List<String> a, List<String> b) {
        Set<String> set = new HashSet<>();
        set.addAll(a);
        set.addAll(b);
        List<String> result = new ArrayList<>();
        result.addAll(set);
        return result;
    }

    public boolean isPermittedToCallForUser(String userId, boolean hasUserObject, String objectId, String method, String type, boolean isStatic, CordraObject co) {
        if ("admin".equals(userId)) return true;
        String creator = null;
        List<String> callPermissionsForMethod = getCallPermissionsForMethodFromDesign(type, isStatic, method);
        if (co != null) {
            //TODO consider isStatic?
            List<String> callPermissionsForMethodForInstance = getCallPermissionsForMethodForInstance(co, method);
            if (callPermissionsForMethodForInstance != null) {
                callPermissionsForMethod = combineOnlyUniqueItems(callPermissionsForMethod, callPermissionsForMethodForInstance);
            }
            if (co.metadata != null) creator = co.metadata.createdBy;
        }
        if (callPermissionsForMethod == null) {
            return false;
        }
        for (String permittedId : callPermissionsForMethod) {
            if ("public".equals(permittedId)) return true;
            if (userId != null && hasUserObject && "authenticated".equals(permittedId)) return true;
            if (permittedId != null && permittedId.equalsIgnoreCase(userId)) return true;
            if (objectId != null && objectId.equalsIgnoreCase(userId) && "self".equals(permittedId)) return true;
            if ("creator".equals(permittedId) && userId != null && userId.equals(creator)) return true;
            if ("writers".equals(permittedId) && canWrite(co, userId, hasUserObject)) return true;
            if ("readers".equals(permittedId) && (canReadOnly(co, userId, hasUserObject) || canWrite(co, userId, hasUserObject))) return true;
        }
        return false;
    }

    private List<String> getCallPermissionsForMethodForInstance(CordraObject co, String method) {
        CordraObject.AccessControlList acl = co.acl;
        if (acl == null) {
            return null;
        }
        Map<String, List<String>> methods = acl.methods;
        if (methods == null) {
            return null;
        }
        List<String> methodAcls = methods.get(method);
        if (methodAcls == null) {
            return null;
        }
        return methodAcls;
    }

    private List<String> getCallPermissionsForMethodFromDesign(String type, boolean isStatic, String method) {
        DefaultAcls acls = authConfig.getAclForObjectType(type);
        if (acls == null) return LEGACY_CALL_PERMISSIONS;
        Map<String, Map<String, List<String>>> aclMethods = acls.aclMethods;
        if (aclMethods == null) {
            return LEGACY_CALL_PERMISSIONS;
        }
        Map<String, List<String>> explicitCallPermissionsForObject = null;
        if (isStatic) {
            explicitCallPermissionsForObject = aclMethods.get("static");
        } else {
            explicitCallPermissionsForObject = aclMethods.get("instance");
        }
        if (explicitCallPermissionsForObject != null) {
            List<String> callPermissionsForMethod = explicitCallPermissionsForObject.get(method);
            if (callPermissionsForMethod != null) {
                return callPermissionsForMethod;
            } else {
                return getDefaultCallPermissionsFor(acls, isStatic);
            }
        } else {
            return getDefaultCallPermissionsFor(acls, isStatic);
        }
    }

    public List<String> getDefaultCallPermissionsFor(DefaultAcls acls, boolean isStatic) {
        if (acls == null) return null;
        Map<String, Map<String, List<String>>> call = acls.aclMethods;
        if (call == null) {
            return null;
        }
        Map<String, List<String>> defaultCallPermissionsForObject = call.get("default");
        if (defaultCallPermissionsForObject == null) {
            return null;
        } else {
            if (isStatic) {
                return defaultCallPermissionsForObject.get("static");
            } else {
                return defaultCallPermissionsForObject.get("instance");
            }
        }
    }

    public boolean isPermittedToCall(String userId, boolean hasUserObject, String objectId, String method, String type) throws CordraException {
        boolean isStatic = type != null;
        CordraObject co = null;
        if (!isStatic) {
            co = storage.get(objectId);
            if (co != null) type = co.type;
        } else {
            String schemaId = cordra.idFromTypeNoSearch(type);
            if (schemaId != null) co = storage.get(schemaId);
        }
        // allow people to know when an object does not exist (if they have default read perm)
        if (co == null && canRead(userId, hasUserObject, null)) return true;
        if (isPermittedToCallForUser(userId, hasUserObject, objectId, method, type, isStatic, co)) return true;
        List<String> groups = getGroupsForUser(userId);
        for (String group : groups) {
            if (isPermittedToCallForUser(group, hasUserObject, objectId, method, type, isStatic, co)) return true;
        }
        return false;
    }

    public boolean isPermittedToCreate(String userId, boolean hasUserObject, String objectType) throws CordraException {
        if (isPermittedToCreateForUser(userId, hasUserObject, objectType)) return true;
        List<String> groups = getGroupsForUser(userId);
        for (String group : groups) {
            if (isPermittedToCreateForUser(group, hasUserObject, objectType)) return true;
        }
        return false;
    }

    public boolean isPermittedToCreate(String userId, boolean hasUserObject, List<String> groups, String objectType) {
        if (isPermittedToCreateForUser(userId, hasUserObject, objectType)) return true;
        for (String group : groups) {
            if (isPermittedToCreateForUser(group, hasUserObject, objectType)) return true;
        }
        return false;
    }

    public List<String> filterTypesPermittedToCreate(String userId, boolean hasUserObject, List<String> allTypes) throws CordraException {
        List<String> result = new ArrayList<>();
        List<String> groups = getGroupsForUser(userId);
        for (String objectType : allTypes) {
            if (isPermittedToCreate(userId, hasUserObject, groups, objectType)) {
                result.add(objectType);
            }
        }
        return result;
    }

    public List<String> getGroupsForUser(String userId) throws CordraException {
        if (userId == null || "admin".equals(userId)) return Collections.emptyList();
        Collection<String> cachedGroups = authCache.getGroupsForUser(userId);
        if (cachedGroups != null) {
            return new ArrayList<>(cachedGroups);
        }
        //String query = "users:\"" + userId + "\"";
        //List<String> res = indexer.searchHandles(query).stream().collect(Collectors.toList());
        Set<String> seen = new HashSet<>();
        Set<String> groupsForUser = getGroupsForUserRecursiveSearch(userId, seen);
        authCache.setGroupsForUser(userId, groupsForUser);
        return new ArrayList<>(groupsForUser);
    }

    public Set<String> getGroupsForUserRecursiveSearch(String memberId, Set<String> seen) throws IndexerException {
        Set<String> accumulation = new HashSet<>();
        Collection<String> groupsFromCache = authCache.getGroupsForUser(memberId);
        if (groupsFromCache != null) {
            accumulation.addAll(groupsFromCache);
            return accumulation;
        }
        String query = "users:\"" + SearchUtil.escape(memberId) + "\" -isVersion:true -objatt_isVersion:true";
        List<String> groupsFromSearch = indexer.searchHandles(query).stream().collect(Collectors.toList());
        //we dont directly cache the groupsFromSearch as it doesn't contain the parents
        if (groupsFromSearch.isEmpty()) {
            authCache.setGroupsForUser(memberId, Collections.emptyList());
        } else {
            for (String groupId : groupsFromSearch) {
                if (!seen.contains(groupId)) {
                    seen.add(groupId);
                    Set<String> groupResult = getGroupsForUserRecursiveSearch(groupId, seen);
                    //authCache.setGroupsForUser(groupId, groupResult);
                    accumulation.add(groupId);
                    accumulation.addAll(groupResult);
                }
            }
        }
        return accumulation;
    }

    //This is more like does is caller in the payloadReaders ACL. They still need read or write as well to read payloads
    private boolean canReadIncludingPayloads(CordraObject co, String userId, boolean hasUserObject) {
        return canPermission(co, userId, hasUserObject, Permission.READ_INCLUDING_PAYLOADS);
    }

    // does not check write, which implies read
    private boolean canReadOnly(CordraObject co, String userId, boolean hasUserObject) {
        return canPermission(co, userId, hasUserObject, Permission.READ);
    }

    private boolean canWrite(CordraObject co, String userId, boolean hasUserObject) {
        return canPermission(co, userId, hasUserObject, Permission.WRITE);
    }

    public CordraObject.AccessControlList getEffectiveAcl(CordraObject co) {
        //TODO should payloadReaders be included here?
        CordraObject.AccessControlList acl = new CordraObject.AccessControlList();
        if (co.acl != null) {
            acl.readers = co.acl.readers;
            acl.writers = co.acl.writers;
        }
        if (acl.readers == null || acl.writers == null) {
            DefaultAcls defaultAcls = getDefaultAcls(co.type);
            if (acl.readers == null) acl.readers = defaultAcls.defaultAclRead;
            if (acl.writers == null) acl.writers = defaultAcls.defaultAclWrite;
        }
        acl.readers = Collections.unmodifiableList(acl.readers);
        acl.writers = Collections.unmodifiableList(acl.writers);
        return acl;
    }

    private boolean canPermission(CordraObject co, String userId, boolean hasUserObject, Permission permission) {
        List<String> acl = null;
        String creator = null;
        if (co != null) {
            if (co.acl != null) {
                if (Permission.WRITE.equals(permission)) {
                    acl = co.acl.writers;
                } else if (Permission.READ.equals(permission)) {
                    acl = co.acl.readers;
                } else if (Permission.READ_INCLUDING_PAYLOADS.equals(permission)) {
                    acl = co.acl.payloadReaders;
                }
            }
            if (co.metadata != null) creator = co.metadata.createdBy;
        }
        if (acl == null && authConfig != null) {
            DefaultAcls defaultAcls = getDefaultAcls(co == null ? null : co.type);
            return isPermittedByDefaultAcls(defaultAcls, co, creator, userId, hasUserObject, permission);
        }
        if (acl == null) {
            acl = Collections.emptyList();
        }
        return isPermittedByAcl(acl, co, creator, userId, hasUserObject, permission);
    }

    public DefaultAcls getDefaultAcls(String objectType) {
        DefaultAcls defaultAcls;
        if (objectType == null) {
            defaultAcls = authConfig.defaultAcls;
        } else {
            defaultAcls = authConfig.schemaAcls.get(objectType);
            if (defaultAcls == null) {
                defaultAcls = authConfig.defaultAcls;
            }
        }
        return defaultAcls;
    }

    private static List<String> defaultDefaultAclPayloadRead = Collections.singletonList("readers");

    private boolean isPermittedByDefaultAcls(DefaultAcls defaultAcls, CordraObject co, String creatorId, String userId, boolean hasUserObject, Permission permission) {
        List<String> acl = null;
        if (Permission.WRITE.equals(permission)) {
            acl = defaultAcls.defaultAclWrite;
        } else if (Permission.READ.equals(permission)) {
            acl = defaultAcls.defaultAclRead;
        } else if (Permission.READ_INCLUDING_PAYLOADS.equals(permission)) {
            acl = defaultAcls.defaultAclPayloadRead;
            if (acl == null) {
                acl = defaultDefaultAclPayloadRead;
            }
        }
        if (acl == null) {
            acl = Collections.emptyList();
        }
        return isPermittedByAcl(acl, co, creatorId, userId, hasUserObject, permission);
    }

    private boolean isPermittedByAcl(List<String> acl, CordraObject co, String creatorId, String userId, boolean hasUserObject, Permission permission) {
        if ("admin".equals(userId)) {
            return true;
        }
        for (String permittedId : acl) {
            if ("public".equals(permittedId)) return true;
            if (userId != null && hasUserObject && "authenticated".equals(permittedId)) return true;
            if (co != null && co.id != null && co.id.equalsIgnoreCase(userId) && "self".equals(permittedId)) return true;
            if (creatorId != null && creatorId.equalsIgnoreCase(userId) && "creator".equals(permittedId)) return true;
            if (permission != Permission.READ) {
                // including "writers" in readers is redundant; writers always have read permission
                // including "readers" in payloadReaders or writers is acceptable
                // we prevent circularity by ignoring readers when checking for read permission.
                // we don't need to check canWrite here, because if the user were a writer we would already have any necessary permission
                if ("readers".equals(permittedId) && canReadOnly(co, userId, hasUserObject)) return true;
            }
            if (permittedId != null && permittedId.equalsIgnoreCase(userId)) return true;
        }
        return false;
    }

    public List<String> listMethodsForUser(String userId, boolean hasUserObject, String type, String objectId, boolean isStatic, boolean includeCrud) throws InterruptedException, ScriptException, CordraException {
        CordraObject co = null;
        if (type == null) {
            if (objectId == null) {
                throw new BadRequestCordraException("Request must include 'type' or 'objectId'");
            }
            co = storage.get(objectId);
            if (co == null) throw new NotFoundCordraException("Missing object: " + objectId);
            type = co.type;
            isStatic = false;
        }
        Set<String> result = new HashSet<>();
        List<String> allMethods = cordra.listMethods(type, isStatic);
        for (String method : allMethods) {
            if (isPermittedToCallForUser(userId, hasUserObject, objectId, method, type, isStatic, co)) {
                result.add(method);
            }
        }
        List<String> groups = getGroupsForUser(userId);
        for (String groupId : groups) {
            for (String method : allMethods) {
                if (!result.contains(method)) {
                    if (isPermittedToCallForUser(groupId, hasUserObject, objectId, method, type, isStatic, co)) {
                        result.add(method);
                    }
                }
            }
        }

        List<String> resultList = new ArrayList<>();
        resultList.addAll(result);
        if (includeCrud) {
            AclEnforcer.Permission perm = permittedOperationsForUserAndGroups(userId, hasUserObject, groups, co);
            List<String> crudOps = crudOpsForPermission(perm);
            resultList.addAll(crudOps);
        }
        return resultList;
    }

    private List<String> crudOpsForPermission(AclEnforcer.Permission perm) {
        List<String> result = new ArrayList<>();
        if (perm == AclEnforcer.Permission.WRITE) {
            result.add("write");
            result.add("delete");
            result.add("payloadRead");
            result.add("read");
        } else if (perm == Permission.READ_INCLUDING_PAYLOADS) {
            result.add("payloadRead");
            result.add("read");
        } else if (perm == AclEnforcer.Permission.READ) {
            result.add("read");
        }
        return result;
    }
}
