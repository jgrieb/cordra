package net.cnri.cordra.storage;

import java.nio.file.Path;
import java.nio.file.Paths;

import com.google.gson.JsonObject;

import net.cnri.cordra.Constants;

public class StorageConfig {
    public String module = "hds"; //mongodb | bdbje | hds | s3 | custom
    public String className; //fully qualified class to custom implementation of CordraStorage, only used when module is "custom"
    public JsonObject options = new JsonObject();

    public static StorageConfig getNewDefaultInstance() {
        StorageConfig storageConfig = new StorageConfig();
        return storageConfig;
    }

    public static Path getBasePathFromOptionsOrSystemProperty(JsonObject options) {
        if (options.has(Constants.CORDRA_DATA_DIR_STORAGE_OPTION)) {
            return Paths.get(options.get(Constants.CORDRA_DATA_DIR_STORAGE_OPTION).getAsString());
        } else {
            String cordraDataPath = System.getProperty(Constants.CORDRA_DATA);
            if (cordraDataPath == null) {
                cordraDataPath = "data";
            }
            return Paths.get(cordraDataPath);
        }
    }
}
