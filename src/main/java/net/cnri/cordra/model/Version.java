package net.cnri.cordra.model;

public class Version {
    public final String number;
    public final long timestamp;
    public final String id;

    private static Version instance;

    private Version() {
        number = System.getProperty("cordra.version.number");
        String timestampString = System.getProperty("cordra.version.timestamp");
        timestamp = Long.valueOf(timestampString);
        id = System.getProperty("cordra.version.id");
    }

    public static synchronized Version getInstance() {
        if (instance == null) {
            if (System.getProperty("cordra.version.number") != null) {
                instance = new Version();
            }
        }
        return instance;
    }
}
