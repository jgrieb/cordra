package net.cnri.cordra.model;

import com.google.gson.JsonElement;
import net.cnri.cordra.api.FacetSpecification;
import net.cnri.cordra.api.SortField;

import java.util.List;
import java.util.Set;

public class SearchRequest {
    public String query;
    public JsonElement queryJson;
    public int pageNum = 0;
    public int pageSize = -1;
    public boolean ids = false;
    public List<SortField> sortFields;
    public Set<String> filter = null;
    public boolean full = true;
    public List<String> filterQueries;
    public List<FacetSpecification> facets;
}
