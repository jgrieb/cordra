package net.cnri.cordra.web.doip;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import com.google.gson.JsonObject;
import net.cnri.cordra.CordraService;
import net.cnri.cordra.CordraServiceFactory;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.NotFoundCordraException;
import net.cnri.cordra.doip.CordraClientDoipProcessor;
import net.cnri.cordra.web.EmptyCheckingHttpServletRequestWrapper;
import net.cnri.cordra.web.ServletUtil;
import net.dona.doip.*;
import net.dona.doip.server.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet({"/doip/*"})
public class DoipPerformOperationServlet extends HttpServlet {

    private static Logger logger = LoggerFactory.getLogger(DoipPerformOperationServlet.class);

    private CordraService cordra;

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            cordra = CordraServiceFactory.getCordraService();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String requestId = req.getParameter("requestId");
        try {
            DoipProcessor doipProcessor = cordra.getDoipProcessor();
            if (doipProcessor == null) {
                sendErrorResponse(resp, requestId, "DOIP API for HTTP clients is not enabled", DoipConstants.STATUS_ERROR);
                return;
            }
            JsonObject doipRequestJson = new JsonObject();
            InDoipMessageFromHttpRequest.augmentRequestFromHttpRequestIfNecessary(doipRequestJson, req);
            String operationId = req.getParameter("operationId");
            if (operationId == null) {
                sendErrorResponse(resp, requestId, "Missing operationId", DoipConstants.STATUS_BAD_REQUEST);
                return;
            }
            String targetId = req.getParameter("targetId");
            if (targetId == null) {
                sendErrorResponse(resp, requestId, "Missing targetId", DoipConstants.STATUS_BAD_REQUEST);
                return;
            }
            JsonObject attributes = DoipRequestUtil.buildAttributesFromQueryParams(req.getParameterMap(), null);
            try {
                if (!"POST".equals(req.getMethod()) && !allowsGet(operationId, targetId, attributes)) {
                    sendErrorResponse(resp, requestId, "GET is not allowed for this operation", DoipConstants.STATUS_DECLINED);
                    return;
                }
            } catch (NotFoundCordraException e) {
                sendErrorResponse(resp, requestId, "Operation not supported", DoipConstants.STATUS_DECLINED);
                return;
            }
            try (
                InDoipMessage inDoipMessage = new InDoipMessageFromJson(doipRequestJson);
                OutDoipMessageHttpResponse outDoipMessage = new OutDoipMessageHttpResponse(resp);
            ) {
                if (attributes.entrySet().size() > 0) {
                    doipRequestJson.add("attributes", attributes);
                    if (!isBasicOperation(operationId)) {
                        doipRequestJson.add("input", attributes); //Note that for GET we copy the attributes into input for custom operations
                        // also for form POST and for empty-body POST
                    }
                }
                DoipServerRequestImpl doipRequest = new DoipServerRequestImpl(inDoipMessage, null, null, null);
                DoipServerResponse doipResponse = new DoipServerHttpResponse(requestId, outDoipMessage);
                doipProcessor.process(doipRequest, doipResponse);
                doipResponse.commit();
            }
        } catch (BadDoipException bx) {
            sendErrorResponse(resp, requestId, "Bad Request", DoipConstants.STATUS_BAD_REQUEST);
        } catch (Exception e) {
            logger.error("Internal error in DOIP API for HTTP clients", e);
            sendErrorResponse(resp, requestId, "Internal error", DoipConstants.STATUS_ERROR);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        if (ServletUtil.isForm(req)) {
            doGet(req, resp);
            return;
        }
        EmptyCheckingHttpServletRequestWrapper reqWrapper = new EmptyCheckingHttpServletRequestWrapper(req);
        if (reqWrapper.isEmpty()) {
            doGet(req, resp);
            return;
        }
        String requestId = req.getParameter("requestId");
        try {
            DoipProcessor doipProcessor = cordra.getDoipProcessor();
            if (doipProcessor == null) {
                sendErrorResponse(resp, requestId, "DOIP API for HTTP clients is not enabled", DoipConstants.STATUS_ERROR);
                return;
            }
            try (
                InDoipMessage inDoipMessage = new InDoipMessageFromHttpRequest(reqWrapper);
                OutDoipMessageHttpResponse outDoipMessage = new OutDoipMessageHttpResponse(resp);
            ) {
                DoipServerRequestImpl doipRequest = new DoipServerRequestImpl(inDoipMessage, null, null, null);
                DoipServerResponse doipResponse = new DoipServerHttpResponse(requestId, outDoipMessage);
                doipProcessor.process(doipRequest, doipResponse);
                doipResponse.commit();
            }
        } catch (BadDoipException bx) {
            sendErrorResponse(resp, requestId, "Bad Request", DoipConstants.STATUS_BAD_REQUEST);
        } catch (Exception e) {
            logger.error("Internal error in DOIP API for HTTP clients", e);
            sendErrorResponse(resp, requestId, "Internal error", DoipConstants.STATUS_ERROR);
        }
    }

    private void sendErrorResponse(HttpServletResponse resp, String requestId, String msg, String doipStatus) {
        try (OutDoipMessageHttpResponse outDoipMessage = new OutDoipMessageHttpResponse(resp)) {
            DoipServerResponse doipResponse = new DoipServerHttpResponse(requestId, outDoipMessage);
            doipResponse.setStatus(doipStatus);
            JsonObject errorOutput = new JsonObject();
            errorOutput.addProperty(DoipConstants.MESSAGE_ATT, msg);
            doipResponse.writeCompactOutput(errorOutput);
            doipResponse.commit();
        } catch (Exception e) {
            logger.error("Exception sending error in DOIP API for HTTP clients", e);
        }
    }

    private boolean isBasicOperation(String operationId) {
        operationId = CordraClientDoipProcessor.unaliasOperation(operationId);
        return basicOperations.contains(operationId);
    }

    private boolean allowsGet(String operationId, String targetId, JsonObject attributes) throws CordraException {
        operationId = CordraClientDoipProcessor.unaliasOperation(operationId);
        if (DoipConstants.OP_HELLO.equals(operationId)) return true;
        if (DoipConstants.OP_LIST_OPERATIONS.equals(operationId)) return true;
        if (DoipConstants.OP_RETRIEVE.equals(operationId)) return true;
        if (DoipConstants.OP_SEARCH.equals(operationId)) return true;
        if (isBasicOperation(operationId)) return false;
        boolean isCalledAsStatic = attributes != null && attributes.has("isCallForType") && attributes.get("isCallForType").getAsBoolean();
        return cordra.callAllowsGet(targetId, operationId, isCalledAsStatic);
    }

    private static final Set<String> basicOperations = new HashSet<>();
    static {
        basicOperations.add(DoipConstants.OP_HELLO);
        basicOperations.add(DoipConstants.OP_LIST_OPERATIONS);
        basicOperations.add(DoipConstants.OP_CREATE);
        basicOperations.add(DoipConstants.OP_RETRIEVE);
        basicOperations.add(DoipConstants.OP_UPDATE);
        basicOperations.add(DoipConstants.OP_DELETE);
        basicOperations.add(DoipConstants.OP_SEARCH);
    }
}
