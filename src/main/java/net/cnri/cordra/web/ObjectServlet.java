package net.cnri.cordra.web;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import net.cnri.cordra.*;
import net.cnri.cordra.api.*;
import net.cnri.cordra.javascript.DirectIo;
import net.cnri.cordra.model.*;
import net.cnri.cordra.util.HttpUtil;
import net.cnri.cordra.util.JacksonUtil;
import net.cnri.cordra.util.JsonUtil;
import net.cnri.util.StreamUtil;
import net.cnri.util.StringUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.util.*;

@WebServlet({"/objects/*"})
@MultipartConfig(fileSizeThreshold=1024*1024)
public class ObjectServlet extends HttpServlet {
    private static Logger logger = LoggerFactory.getLogger(ObjectServlet.class);
    private static final String JSON = "json";
    private static final String CONTENT = "content";
    private static final String ACL = "acl";
    private static final String USER_METADATA = "userMetadata";

    private CordraService cordra;
    private Gson gson;
    private Gson prettyGson;

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            gson = GsonUtility.getGson();
            prettyGson = GsonUtility.getPrettyGson();
            cordra = CordraServiceFactory.getCordraService();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        String objectId = ServletUtil.getPath(req);
        String userId = (String) req.getAttribute("userId");
        if (objectId != null && !objectId.isEmpty()) objectId = objectId.substring(1);
        if (objectId == null || objectId.isEmpty()) {
            String query = req.getParameter("query");
            String queryJson = req.getParameter("queryJson");
            if (query == null && queryJson == null) {
                ServletErrorUtil.badRequest(resp, "Missing object id in GET");
                return;
            } else {
                doSearch(req, resp);
                return;
            }
        }
        String jsonPointer = req.getParameter("jsonPointer");
        String filterJson = req.getParameter("filter");
        String payload = req.getParameter("payload");
        boolean isWantText = ServletUtil.getBooleanParameter(req, "text");
        boolean isFull = ServletUtil.getBooleanParameter(req, "full");
        boolean isPretty = ServletUtil.getBooleanParameter(req, "pretty", false);
        boolean hasResponseContext = ServletUtil.getBooleanParameter(req, "includeResponseContext");

        if (payload != null) {
            boolean metadata = ServletUtil.getBooleanParameter(req, "metadata");
            if (metadata) {
                doGetPayloadMetadata(req, resp, objectId, payload, userId);
            } else {
                doGetPayload(req, resp, objectId, payload, userId);
            }
        } else if (jsonPointer == null && filterJson == null) {
            if (isFull) {
                getObjectPlusMetadata(resp, objectId, userId, isPretty, hasResponseContext);
            } else {
                doGetWholeObject(resp, objectId, userId, isWantText, isPretty);
            }
        } else {
            if (jsonPointer != null) {
                if (JsonUtil.isValidJsonPointer(jsonPointer)) {
                    doGetJsonPointer(resp, objectId, userId, jsonPointer, isWantText, isPretty, isFull);
                } else {
                    ServletErrorUtil.badRequest(resp, "Invalid JSON Pointer " + jsonPointer);
                }
            } else {
                doGetFilterByJsonPointers(resp, objectId, userId, filterJson, isPretty, isFull);
            }
        }
    }

    private void getObjectPlusMetadata(HttpServletResponse resp, String objectId, String userId, boolean isPretty, boolean hasResponseContext) throws IOException {
        try {
            CordraObject co = cordra.getContentPlusMetaWithPostProcessing(objectId, userId);
            if (co.content == null || co.type == null) {
                ServletErrorUtil.notFound(resp, "Not a valid cordra object");
                return;
            }
            resp.setHeader("Location", StringUtils.encodeURLPath("/objects/" + co.id));
            resp.setHeader("X-Schema", co.type);
            if (hasResponseContext) {
                String perm = resp.getHeader("X-Permission");
                if (perm != null) {
                    if (co.responseContext == null) co.responseContext = new JsonObject();
                    co.responseContext.addProperty("permission", perm);
                }
            }
            String json;
            if (isPretty) {
                json = prettyGson.toJson(co);
            } else {
                json = gson.toJson(co);
            }
            resp.getWriter().write(json);
        } catch (InternalErrorCordraException e) {
            logger.error("Something went wrong getting " + objectId, e);
            ServletErrorUtil.internalServerError(resp);
        } catch (CordraException e) {
            ServletErrorUtil.writeCordraException(resp, e);
        } catch (Exception e) {
            logger.error("Something went wrong getting " + objectId, e);
            ServletErrorUtil.internalServerError(resp);
        }
    }

    private void doGetWholeObject(HttpServletResponse resp, String objectId, String userId, boolean isWantText, boolean isPretty) throws IOException {
        try {
            CordraObject co = cordra.getContentPlusMetaWithPostProcessing(objectId, userId);
            if (co.content == null || co.type == null) {
                ServletErrorUtil.notFound(resp, "Not a valid cordra object");
                return;
            }
            resp.setHeader("Location", StringUtils.encodeURLPath("/objects/" + co.id));
            resp.setHeader("X-Schema", co.type);
            if (isWantText) {
                JsonNode jsonNode = JacksonUtil.gsonToJackson(co.content);
                String mediaType = cordra.getMediaType(co.type, jsonNode, "");
                writeText(resp, jsonNode, mediaType);
            } else {
                String jsonData;
                if (isPretty) {
                    jsonData = prettyGson.toJson(co.content);
                } else {
                    jsonData = co.content.toString();
                }
                resp.getWriter().write(jsonData);
            }
        } catch (InternalErrorCordraException e) {
            logger.error("Something went wrong getting " + objectId, e);
            ServletErrorUtil.internalServerError(resp);
        } catch (CordraException e) {
            ServletErrorUtil.writeCordraException(resp, e);
        } catch (Exception e) {
            logger.error("Something went wrong getting " + objectId, e);
            ServletErrorUtil.internalServerError(resp);
        }
    }

    private void doGetPayloadMetadata(@SuppressWarnings("unused") HttpServletRequest req, HttpServletResponse resp, String objectId, String payloadName, String userId) throws IOException {
        try {
            FileMetadataResponse metadataResponse = cordra.getPayloadMetadata(objectId, payloadName, userId);
            gson.toJson(metadataResponse, resp.getWriter());
        } catch (InternalErrorCordraException e) {
            logger.error("Something went wrong getting " + objectId, e);
            ServletErrorUtil.internalServerError(resp);
        } catch (CordraException e) {
            ServletErrorUtil.writeCordraException(resp, e);
        } catch (Exception e) {
            logger.error("Something went wrong getting " + objectId, e);
            ServletErrorUtil.internalServerError(resp);
        }
    }

    private void doGetPayload(HttpServletRequest req, HttpServletResponse resp, String objectId, String payloadName, String userId) throws IOException {
        try {
            Range range = getRangeFromRequest(req);
            CordraObject co = cordra.getCordraObjectForPayload(objectId, payloadName, range.getStart(), range.getEnd(), userId);
            Payload payload = CordraService.getCordraObjectPayloadByName(co, payloadName);
            if (payload == null) {
                ServletErrorUtil.notFound(resp, "No payload " + payloadName + " in object " + objectId);
                return;
            }
            if (setPayloadResponseHeadersReturnRangeOkay(req, resp, payload, range)) {
                DirectIo directIo = new DirectIoForPayload(req, resp);
                cordra.streamPayload(co, payload, range, userId, directIo);
            }
        } catch (InternalErrorCordraException e) {
            logger.error("Something went wrong getting " + objectId, e);
            ServletErrorUtil.internalServerError(resp);
        } catch (CordraException e) {
            ServletErrorUtil.writeCordraException(resp, e);
        } catch (Exception e) {
            logger.error("Something went wrong getting " + objectId, e);
            ServletErrorUtil.internalServerError(resp);
        }
    }

    private void doGetFilterByJsonPointers(HttpServletResponse resp, String objectId, String userId, String filterJson, boolean isPretty, boolean isFull) throws IOException {
        try {
            Set<String> filter = gson.fromJson(filterJson, new TypeToken<Set<String>>(){}.getType());
            JsonElement jsonElement = cordra.getObjectFilterByJsonPointers(objectId, userId, filter, isFull);
            String json;
            if (isPretty) {
                json = GsonUtility.getPrettyGson().toJson(jsonElement);
            } else {
                json = jsonElement.toString();
            }
            resp.getWriter().write(json);
        } catch (InternalErrorCordraException e) {
            logger.error("Something went wrong getting " + objectId, e);
            ServletErrorUtil.internalServerError(resp);
        } catch (CordraException e) {
            ServletErrorUtil.writeCordraException(resp, e);
        } catch (Exception e) {
            logger.error("Something went wrong getting " + objectId, e);
            ServletErrorUtil.internalServerError(resp);
        }
    }

    private void doGetJsonPointer(HttpServletResponse resp, String objectId, String userId, String jsonPointer, boolean isWantText, boolean isPretty, boolean isFull) throws IOException {
        try {
            JsonNode jsonNode = cordra.getAtJsonPointer(objectId, userId, jsonPointer, isFull);

            if (jsonNode == null) {
                ServletErrorUtil.notFound(resp, "Missing data at jsonPointer in object");
            } else {
                if (isWantText) {
                    CordraObject co = cordra.getCordraObject(objectId);
                    String mediaType = cordra.getMediaType(co.type, JacksonUtil.gsonToJackson(co.content), jsonPointer);
                    writeText(resp, jsonNode, mediaType);
                } else {
                    if (isPretty) {
                        JacksonUtil.prettyPrintJson(resp.getWriter(), jsonNode);
                    } else {
                        JacksonUtil.printJson(resp.getWriter(), jsonNode);
                    }
                }
            }
        } catch (InternalErrorCordraException e) {
            logger.error("Something went wrong getting " + objectId, e);
            ServletErrorUtil.internalServerError(resp);
        } catch (CordraException e) {
            ServletErrorUtil.writeCordraException(resp, e);
        } catch (Exception e) {
            logger.error("Something went wrong getting " + objectId, e);
            ServletErrorUtil.internalServerError(resp);
        }
    }

    Range getRangeFromRequest(HttpServletRequest req) {
        if (req.getHeader("If-Range") != null) return new Range(null, null);
        String rangeHeader = req.getHeader("Range");
        if (rangeHeader == null) return new Range(null, null);
        if (!rangeHeader.startsWith("bytes=")) return new Range(null, null);
        if (rangeHeader.contains(",")) return new Range(null, null);
        String rangePart = rangeHeader.substring(6);
        String[] parts = rangePart.split("-");
        if (parts.length == 1 && rangePart.endsWith("-")) {
            return new Range(Long.parseLong(parts[0]), null);
        }

        if (parts.length != 2) return new Range(null, null);
        try {
            if (parts[0].isEmpty()) {
                return new Range(null, Long.parseLong(parts[1]));
            } else if (parts[1].isEmpty()) {
                return new Range(Long.parseLong(parts[0]), null);
            } else {
                return new Range(Long.parseLong(parts[0]), Long.parseLong(parts[1]));
            }
        } catch (NumberFormatException e) {
            return new Range(null, null);
        }
    }

    private boolean setPayloadResponseHeadersReturnRangeOkay(HttpServletRequest req, HttpServletResponse resp, Payload payload, Range range) {
        String mediaType = payload.mediaType;
        String filename = payload.filename;
        long size = payload.size;
        if (size == 0) {
            // ignore range request for empty payload
            resp.setContentLength(0);
        } else if (size > 0) {
            long contentLength = size;
            if (range.isPartial()) {
                if (!range.isSatisfiable(size)) {
                    resp.setStatus(HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE);
                    resp.setHeader("Content-Range", "bytes */" + size);
                    return false;
                } else {
                    range = range.withSize(size);
                    resp.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);
                    resp.setHeader("Content-Range", "bytes " + range.getStart() + "-" + range.getEnd() + "/" + size);
                    contentLength = range.getEnd() - range.getStart() +1;
                }
            }
            if (contentLength <= Integer.MAX_VALUE) {
                resp.setContentLength((int)contentLength);
            }
        } else if (size < 0) {
            // unknown size
            if (range.isPartial()) {
                // TODO consider allowing onPayloadResolution to specify partialness and content-range
                resp.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);
            }
        }
        if (mediaType == null) {
            mediaType = "application/octet-stream";
        }
        String disposition = req.getParameter("disposition");
        if (disposition == null) disposition = "inline";
        resp.setHeader("Content-Disposition", HttpUtil.contentDispositionHeaderFor(disposition, filename));
        resp.setContentType(mediaType);
        return true;
    }

    private void writeText(HttpServletResponse resp, JsonNode jsonNode, String mediaType) throws IOException {
        if (jsonNode.isValueNode()) {
            if (mediaType == null) {
                mediaType = "text/plain";
            }
            resp.setContentType(mediaType);
            if (!mediaType.contains(";")) {
                resp.setCharacterEncoding("UTF-8");
            }
            resp.getWriter().write(jsonNode.asText());
        } else {
            ServletErrorUtil.badRequest(resp, "Requested json is not textual");
        }
    }

    private void doSearch(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/search");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        createOrUpdate(req, resp, false);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (ServletUtil.isForm(req)) {
            doGet(req, resp);
        } else {
            createOrUpdate(req, resp, true);
        }
    }

    private void createOrUpdate(HttpServletRequest req, HttpServletResponse resp, boolean isCreate) throws IOException {
        // Note: due to longstanding Tomcat behaviour, ensure that getParts() is called before getParameter() for multipart PUT requests
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        //PathParser path = new PathParser(req.getPathInfo());
        String objectId = ServletUtil.getPath(req);
        if (objectId != null && !objectId.isEmpty()) objectId = objectId.substring(1);
        else objectId = null;

        if (isCreate && objectId != null && !objectId.isEmpty()) {
            ServletErrorUtil.badRequest(resp, "Unexpected object id in POST");
            return;
        }
        if (!isCreate && (objectId == null || objectId.isEmpty())) {
            ServletErrorUtil.badRequest(resp, "Missing object id in PUT");
            return;
        }

        String handle = null;
        List<Payload> payloads = null;
        try {
            Request requestData = getJsonAndPayloadsFromRequest(req);
            boolean isDryRun = ServletUtil.getBooleanParameter(req, "dryRun");
            String objectType = req.getParameter("type");
            if (objectType != null && !cordra.isKnownType(objectType)) {
                ServletErrorUtil.badRequest(resp, "Unknown type " + objectType);
                return;
            }
            if (isCreate) {
                if (objectType == null || objectType.isEmpty()) {
                    ServletErrorUtil.badRequest(resp, "Missing object type");
                    return;
                }
            }
            List<String> payloadsToDelete = new ArrayList<>();
            String[] payloadsToDeleteArray = req.getParameterValues("payloadToDelete");
            if (payloadsToDeleteArray != null) {
                for (String payloadName : payloadsToDeleteArray) {
                    payloadsToDelete.add(payloadName);
                }
            }
            handle = req.getParameter("handle");
            if (handle == null) {
                String suffix = req.getParameter("suffix");
                if (suffix != null) handle = cordra.getHandleForSuffix(suffix);
            }
            String jsonData = requestData.json;
            String aclString = requestData.acl;
            String userMetadataString = requestData.userMetadata;
            payloads = requestData.payloads;
            if (payloads != null) {
                for (Payload payload : payloads) {
                    payloadsToDelete.remove(payload.name);
                }
            }
            if (jsonData == null) {
                ServletErrorUtil.badRequest(resp, "Missing JSON");
                return;
            }
            CordraObject.AccessControlList acl = null;
            if (aclString != null) {
                try {
                    acl = gson.fromJson(aclString, CordraObject.AccessControlList.class);
                } catch (JsonParseException e) {
                    ServletErrorUtil.badRequest(resp, "Invalid ACL format");
                    return;
                }
            }
            JsonObject userMetadata = null;
            if (userMetadataString != null) {
                try {
                    userMetadata = JsonParser.parseString(userMetadataString).getAsJsonObject();
                } catch (Exception e) {
                    ServletErrorUtil.badRequest(resp, "Invalid userMetadata format");
                    return;
                }
            }
            CordraObject co;
            String userId = (String) req.getAttribute("userId");
            boolean hasUserObject = ServletUtil.getBooleanAttribute(req, "hasUserObject");
            String jsonPointer = req.getParameter("jsonPointer");
            if (isCreate) {
                co = cordra.writeJsonAndPayloadsIntoCordraObjectIfValid(objectType, jsonData, acl, userMetadata, payloads, handle, userId, isDryRun);
            } else {
                if (jsonPointer != null) {
                    co = cordra.modifyObjectAtJsonPointer(objectId, jsonPointer, jsonData, userId, hasUserObject, isDryRun);
                } else {
                    co = cordra.writeJsonAndPayloadsIntoCordraObjectIfValidAsUpdate(objectId, objectType, jsonData, acl, userMetadata, payloads, userId, hasUserObject, payloadsToDelete, isDryRun);
                }
            }

            resp.setStatus(HttpServletResponse.SC_OK);
            resp.setHeader("Location", StringUtils.encodeURLPath("/objects/" + co.id));
            resp.setHeader("X-Schema", co.type);
            boolean isFull = ServletUtil.getBooleanParameter(req, "full");
            if (isFull) {
//                if (co.metadata != null) co.metadata.internalMetadata = null;
                co = CordraService.copyOfCordraObjectRemovingInternalMetadata(co);
                boolean hasResponseContext = ServletUtil.getBooleanParameter(req, "includeResponseContext");
                if (hasResponseContext) {
                    String perm = resp.getHeader("X-Permission");
                    if (perm != null) {
                        if (co.responseContext == null) co.responseContext = new JsonObject();
                        co.responseContext.addProperty("permission", perm);
                    }
                }
                String fullJson = gson.toJson(co);
                resp.getWriter().write(fullJson);
            } else if (!isCreate && jsonPointer != null) {
                JsonNode node = JacksonUtil.gsonToJackson(co.content);
                node = JacksonUtil.getJsonAtPointer(jsonPointer, node);
                JacksonUtil.printJson(resp.getWriter(), node);
            } else {
                resp.getWriter().write(co.getContentAsString());
            }
        } catch (InternalErrorCordraException e) {
            logger.error("Unexpected exception in doPost", e);
            ServletErrorUtil.internalServerError(resp);
        } catch (ReadOnlyCordraException e) {
            ServletErrorUtil.badRequest(resp, "Cordra is read-only");
        } catch (CordraException e) {
            ServletErrorUtil.writeCordraException(resp, e);
        } catch (InvalidException e) {
            ServletErrorUtil.badRequest(resp, e.getMessage());
        } catch (Exception e) {
            logger.error("Unexpected exception in doPost", e);
            ServletErrorUtil.internalServerError(resp);
        } finally {
            if (payloads != null) {
                for (Payload payload : payloads) {
                    try { payload.getInputStream().close(); } catch (Exception e) { }
                }
            }
        }
    }

    private static class Request {
        String json;
        String acl;
        String userMetadata;
        List<Payload> payloads;

        public Request(String json) {
            this.json = json;
        }

        public Request(String json, String acl, String userMetadata, List<Payload> payloads) {
            this.json = json;
            this.acl = acl;
            this.userMetadata = userMetadata;
            this.payloads = payloads;
        }
    }

    private Request getJsonAndPayloadsFromRequest(HttpServletRequest req) throws IOException, ServletException {
        // Note: due to longstanding Tomcat behaviour, ensure that getParts() is called before getParameter() for multipart PUT requests
        if (isMultipart(req)) {
            String jsonData = null;
            String acl = null;
            String userMetadata = null;
            List<Payload> payloads = new ArrayList<>();
            Collection<Part> parts = req.getParts();
            for(Part part : parts) {
                String partName = part.getName();
                String filename = HttpUtil.getContentDispositionFilename(part.getHeader("Content-Disposition"));
                if ((JSON.equals(partName) || CONTENT.equals(partName)) && filename == null) {
                    jsonData = getPartAsString(part);
                } else if (ACL.equals(partName) && filename == null) {
                    acl = getPartAsString(part);
                } else if (USER_METADATA.equals(partName) && filename == null) {
                    userMetadata = getPartAsString(part);
                } else {
                    String payloadName = partName;
                    if (filename != null) {
                        // form-data without filename treated as parameters, so does not generate a payload
                        Payload payload = new Payload();
                        payload.name = payloadName;
                        payload.setInputStream(part.getInputStream());
                        payload.mediaType = part.getContentType();
                        payload.filename = filename;
                        payloads.add(payload);
                    }
                }
            }
            return new Request(jsonData, acl, userMetadata, payloads);
        } else if (ServletUtil.isForm(req) && "POST".equals(req.getMethod())) {
            String content = req.getParameter(CONTENT);
            if (content == null) content = req.getParameter(JSON);
            return new Request(content);
        } else if (ServletUtil.isForm(req)) {
            return new Request(getJsonFromFormData(StreamUtil.readFully(req.getReader())));
        } else {
            return new Request(StreamUtil.readFully(req.getReader()));
        }
    }

    static String getJsonFromFormData(String data) {
        String[] strings = data.split("&");
        for (String string : strings) {
            int equals = string.indexOf('=');
            if (equals < 0) {
                String name = StringUtils.decodeURL(string);
                if (JSON.equals(name) || CONTENT.equals(name)) return "";
            } else {
                String name = StringUtils.decodeURL(string.substring(0, equals));
                if (JSON.equals(name) || CONTENT.equals(name)) {
                    String value = StringUtils.decodeURL(string.substring(equals + 1));
                    return value;
                }
            }
        }
        return null;
    }

    private static boolean isMultipart(HttpServletRequest req) {
        String contentType = req.getContentType();
        if (contentType == null) return false;
        contentType = contentType.toLowerCase(Locale.ENGLISH);
        return contentType.startsWith("multipart/form-data");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        String userId = (String) req.getAttribute("userId");
        String objectId = ServletUtil.getPath(req);
        if (objectId != null && !objectId.isEmpty()) objectId = objectId.substring(1);
        if (objectId == null) objectId = "";
        boolean deleteObjectWithEmptyId = ServletUtil.getBooleanParameter(req, "deleteObjectWithEmptyId", false);
        if (deleteObjectWithEmptyId && !objectId.isEmpty()) {
            ServletErrorUtil.badRequest(resp, "Specified object id to delete but also deleteObjectWithEmptyId");
            return;
        }

        if (objectId.isEmpty() && !deleteObjectWithEmptyId) {
            ServletErrorUtil.badRequest(resp, "Missing object id in DELETE");
            return;
        }
        String jsonPointer = req.getParameter("jsonPointer");
        String payloadName = req.getParameter("payload");
        if (jsonPointer != null && payloadName != null) {
            ServletErrorUtil.badRequest(resp, "DELETE specified both jsonPointer and payload");
            return;
        }
        if ("".equals(jsonPointer)) {
            ServletErrorUtil.badRequest(resp, "Cannot delete empty json pointer");
            return;
        }
        boolean hasUserObject = ServletUtil.getBooleanAttribute(req, "hasUserObject");
        if (jsonPointer == null && payloadName == null) {
            try {
                cordra.delete(objectId, userId);
                resp.getWriter().write("{}");
            } catch (InternalErrorCordraException e) {
                logger.error("Unexpected exception in doDelete", e);
                ServletErrorUtil.internalServerError(resp);
            } catch (ReadOnlyCordraException e) {
                ServletErrorUtil.badRequest(resp, "Cordra is read-only");
            } catch (CordraException e) {
                ServletErrorUtil.writeCordraException(resp, e);
            } catch (Exception e) {
                logger.error("Unexpected exception in doDelete", e);
                ServletErrorUtil.internalServerError(resp);
            }
        } else {
            try {
                if (jsonPointer != null) {
                    cordra.deleteJsonPointer(objectId, jsonPointer, userId, hasUserObject);
                }
                if (payloadName != null) {
                    cordra.deletePayload(objectId, payloadName, userId, hasUserObject);
                }
                resp.getWriter().write("{}");
            } catch (InternalErrorCordraException e) {
                logger.error("Unexpected exception in doDelete", e);
                ServletErrorUtil.internalServerError(resp);
            } catch (ReadOnlyCordraException e) {
                ServletErrorUtil.badRequest(resp, "Cordra is read-only");
            } catch (CordraException e) {
                ServletErrorUtil.writeCordraException(resp, e);
            } catch (Exception e) {
                logger.error("Unexpected exception in doDelete", e);
                ServletErrorUtil.internalServerError(resp);
            }
        }
    }

    private static String getPartAsString(Part part) throws IOException {
        InputStream in = part.getInputStream();
        String result = IOUtils.toString(in, "UTF-8").trim();
        in.close();
        return result;
    }


    private static class DirectIoForPayload implements DirectIo {
        private final HttpServletRequest req;
        private final HttpServletResponse resp;
        private boolean hasSetCharacterEncoding;

        public DirectIoForPayload(HttpServletRequest req, HttpServletResponse resp) {
            this.req = req;
            this.resp = resp;
        }

        @Override
        public InputStream getInputAsInputStream() throws IOException {
            throw new UnsupportedOperationException();
        }

        @Override
        public Reader getInputAsReader() throws IOException {
            throw new UnsupportedOperationException();
        }

        @Override
        public OutputStream getOutputAsOutputStream() throws IOException {
            return resp.getOutputStream();
        }

        @Override
        public Writer getOutputAsWriter() throws IOException {
            if (!hasSetCharacterEncoding) {
                resp.setCharacterEncoding("UTF-8");
                hasSetCharacterEncoding = true;
            }
            return resp.getWriter();
        }

        @Override
        public String getInputMediaType() {
            throw new UnsupportedOperationException();
        }

        @Override
        public String getInputFilename() {
            throw new UnsupportedOperationException();
        }

        @Override
        public void setOutputMediaType(String mediaType) {
            if (mediaType.toLowerCase(Locale.ROOT).contains("charset")) {
                hasSetCharacterEncoding = true;
            }
            resp.setContentType(mediaType);
        }

        @Override
        public void setOutputFilename(String filename) {
            String disposition = req.getParameter("disposition");
            if (disposition == null) disposition = "inline";
            resp.setHeader("Content-Disposition", HttpUtil.contentDispositionHeaderFor(disposition, filename));
        }
    }
}
