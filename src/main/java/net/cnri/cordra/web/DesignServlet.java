package net.cnri.cordra.web;

import net.cnri.cordra.CordraService;
import net.cnri.cordra.CordraServiceFactory;
import net.cnri.cordra.DesignPlusSchemas;
import net.cnri.cordra.GsonUtility;
import net.cnri.cordra.api.CordraException;

import javax.script.ScriptException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

@WebServlet({"/design/*"})
public class DesignServlet extends HttpServlet {
    private static Logger logger = LoggerFactory.getLogger(DesignServlet.class);

    private static CordraService cordra;

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            cordra = CordraServiceFactory.getCordraService();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        String userId = (String) req.getAttribute("userId");
        boolean hasUserObject = ServletUtil.getBooleanAttribute(req, "hasUserObject");
        try {
            DesignPlusSchemas design = cordra.getDesignAsUser(userId, hasUserObject);
            GsonUtility.getPrettyGson().toJson(design, resp.getWriter());
        } catch (CordraException | ScriptException | InterruptedException e) {
            logger.error("Exception in GET /design", e);
            ServletErrorUtil.internalServerError(resp);
        }
    }
}
