package net.cnri.cordra.web;

import net.cnri.cordra.CordraService;
import net.cnri.cordra.CordraServiceFactory;
import net.cnri.cordra.InvalidException;
import net.cnri.cordra.api.BadRequestCordraException;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.InternalErrorCordraException;
import net.cnri.cordra.javascript.DirectIo;
import net.cnri.cordra.util.HttpUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

@WebServlet({"/call/*"})
public class CallServlet extends HttpServlet {
    private static final Logger logger = LoggerFactory.getLogger(CallServlet.class);

    private static CordraService cordra;

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            cordra = CordraServiceFactory.getCordraService();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGetOrPost(req, resp, true);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGetOrPost(req, resp, false);
    }

    private void doGetOrPost(HttpServletRequest req, HttpServletResponse resp, boolean isGet) throws IOException {
        try {
            String objectId = req.getParameter("objectId");
            String type = req.getParameter("type");
            if (type == null && objectId == null) throw new BadRequestCordraException("objectId or type required");
            String method = req.getParameter("method");
            if (method== null) throw new BadRequestCordraException("method required");
            String userId = (String) req.getAttribute("userId");
            boolean hasUserObject = ServletUtil.getBooleanAttribute(req, "hasUserObject");
            String result = cordra.call(objectId, type, userId, hasUserObject, method, new DirectIoForCallServlet(req, resp), isGet);
            if (result != null) {
                // don't set content-type for empty response
                resp.setContentType("application/json");
                resp.setCharacterEncoding("UTF-8");
                resp.getWriter().write(result);
            }
        } catch (InternalErrorCordraException e) {
            logger.error("Unexpected error calling method", e);
            ServletErrorUtil.internalServerError(resp);
        } catch (CordraException e) {
            ServletErrorUtil.writeCordraException(resp, e);
        } catch (InvalidException e) {
            ServletErrorUtil.badRequest(resp, e.getMessage());
        } catch (Exception e) {
            logger.error("Unexpected error calling method", e);
            ServletErrorUtil.internalServerError(resp);
        }
    }

    public static class DirectIoForCallServlet implements DirectIo {

        private final EmptyCheckingHttpServletRequestWrapper req;
        private final HttpServletResponse resp;
        private String params;
        private InputStream paramsInputStream;
        private Reader paramsReader;
        private boolean hasSetCharacterEncoding;

        public DirectIoForCallServlet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
            this.req = new EmptyCheckingHttpServletRequestWrapper(req);
            this.resp = resp;
            if ("GET".equals(req.getMethod()) || this.req.isEmpty()) {
                params = req.getParameter("params");
                if (params == null) params = "";
            }
        }

        @Override
        public InputStream getInputAsInputStream() throws IOException {
            if (params != null) {
                if (paramsReader != null) throw new IllegalStateException("Cannot use both getInputStream and getReader");
                if (paramsInputStream == null) paramsInputStream = new ByteArrayInputStream(params.getBytes(StandardCharsets.UTF_8));
                return paramsInputStream;
            }
            return req.getInputStream();
        }

        @Override
        public Reader getInputAsReader() throws IOException {
            if (params != null) {
                if (paramsInputStream != null) throw new IllegalStateException("Cannot use both getInputStream and getReader");
                if (paramsReader == null) paramsReader = new StringReader(params);
                return paramsReader;
            }
            return req.getReader();
        }

        @Override
        public OutputStream getOutputAsOutputStream() throws IOException {
            return resp.getOutputStream();
        }

        @Override
        public Writer getOutputAsWriter() throws IOException {
            if (!hasSetCharacterEncoding) {
                resp.setCharacterEncoding("UTF-8");
                hasSetCharacterEncoding = true;
            }
            return resp.getWriter();
        }

        @Override
        public String getInputMediaType() {
            if (params != null && !params.isEmpty()) return null;
            return req.getContentType();
        }

        @Override
        public String getInputFilename() {
            if (params != null && !params.isEmpty()) return null;
            return HttpUtil.getContentDispositionFilename(req.getHeader("Content-Disposition"));
        }

        @Override
        public void setOutputMediaType(String mediaType) {
            if (mediaType.toLowerCase(Locale.ROOT).contains("charset")) {
                hasSetCharacterEncoding = true;
            }
            resp.setContentType(mediaType);
        }

        @Override
        public void setOutputFilename(String filename) {
            String disposition = req.getParameter("disposition");
            if (disposition == null) disposition = "inline";
            resp.setHeader("Content-Disposition", HttpUtil.contentDispositionHeaderFor(disposition, filename));
        }
    }
}
